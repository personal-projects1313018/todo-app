// Select elements from HTML to target
let form = document.getElementById("form");
let textInput = document.getElementById("textInput");
let dateInput = document.getElementById("dateInput");
let textarea = document.getElementById("textarea");
let msg = document.getElementById("msg");
let tasks = document.getElementById("tasks");
let add = document.getElementById("add");

// adding current date to the header
const currentDate = new Date();
const day = currentDate.getDate();
const month = currentDate.toLocaleString("default", { month: "long" });

// adding current year to the header
const year = currentDate.getFullYear();

// Set the text content for the date and year
document.getElementById("currentDate").textContent = `${day} ${month}`;
document.getElementById("currentYear").textContent = year;

// Event listener for form submission
form.addEventListener("submit", (e) => {
    e.preventDefault();
    formValidation();
  });
  
let formValidation = () => {
    if (textInput.value === "") {
      console.log("failure");
      msg.innerHTML = "Task cannot be blank";
    } else {
      console.log("success");
      msg.innerHTML = "";
    }

    // Accept data and store in local storage
    acceptData();
    // Close modal after submission
    add.setAttribute("data-bs-dismiss", "modal");
    add.click();

    (() => {
        add.setAttribute("data-bs-dismiss", "");
        })();
};

// Collect data and store in local storage
let data = [];

let acceptData = () => {
    data.push({
        text: textInput.value,
        date: dateInput.value,
        description: textarea.value
    });

    localStorage.setItem("data", JSON.stringify(data));
    console.log(data);

    // Create new task
    createTasks();
};

// Create new task (allows task with only title and date, description is optional)
let createTasks = (text, date) => {
    tasks.innerHTML = "";
    data.map((x, y) => {
        return tasks.innerHTML += `
        <div id=${y} class="card">
            <span class="fw-bold card-title">${x.text}</span>
            <span class="small text-secondary card-date">${x.date}</span>
            <p class="card-text">${x.description}</p>

            <span class="options">
                <i onClick="editTask(this)" data-bs-toggle="modal" data-bs-target="#form" class="fas fa-edit"></i>
                <i onClick="deleteTask(this);createTasks()" class="fas fa-trash"></i>
            </span>
        </div>
        `;
    });

    resetForm();
};

// Reset form after submission
let resetForm = () => {
    textInput.value = "";
    dateInput.value = "";
    textarea.value = "";
};

// Delete task
let deleteTask = (e) => {
    e.parentElement.parentElement.remove();
    data.splice(e.parentElement.parentElement.id, 1);

    localStorage.setItem("data", JSON.stringify(data));

    console.log(data);
};

// Edit task
let editTask = (e) => {
    let selectedTask = e.parentElement.parentElement;

    textInput.value = selectedTask.children[0].innerHTML;
    dateInput.value = selectedTask.children[1].innerHTML;
    textarea.value = selectedTask.children[2].innerHTML;

    deleteTask(e);
};

// Prevent data from being lost on page refresh by running a IIFE (Immediately Invoked Function Expression)
(() => {
    data = JSON.parse(localStorage.getItem("data")) || [];
    console.log(data);
    createTasks();
})();